
package app.crossword.yourealwaysbe.forkyz.settings;

import java.util.Objects;

public enum PlayQuickAction {
    NONE("NONE"),
    REVEAL_LETTER("REVEAL_LETTER");

    private String settingsValue;

    PlayQuickAction(String settingsValue) {
        this.settingsValue = settingsValue;
    }

    public String getSettingsValue() {
        return settingsValue;
    }

    public static PlayQuickAction getFromSettingsValue(String value) {
        for (PlayQuickAction o : PlayQuickAction.values()) {
            if (Objects.equals(value, o.getSettingsValue()))
                return o;
        }
        return PlayQuickAction.NONE;
    }
}
